package com.jetpack.dogs.view.fragment

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.jetpack.dogs.R

class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}