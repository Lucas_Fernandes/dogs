package com.jetpack.dogs.view.activity

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.jetpack.dogs.R
import com.jetpack.dogs.utils.Const.PERMISSION_SEND_SMS
import com.jetpack.dogs.view.fragment.DetailFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity() {

    private lateinit var navController: NavController
    private val sendSmsPermission = Manifest.permission.SEND_SMS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.fragment)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean =
        NavigationUI.navigateUp(navController, null)


    fun checkSmsPermission() {
        if (ContextCompat.checkSelfPermission(this, sendSmsPermission) !=
            PackageManager.PERMISSION_GRANTED) {
            checkShouldRequestPermissionRationale()
        } else {
            notifyDetailFragment(true)
        }
    }

    private fun checkShouldRequestPermissionRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, sendSmsPermission)) {
            AlertDialog.Builder(this)
                .setTitle(getString(R.string.send_sms_permission_text))
                .setMessage(getString(R.string.request_rationale_message))
                .setPositiveButton(getString(R.string.yes)) { _, _ ->
                    requestSmsPermission()
                }
                .setNegativeButton(getString(R.string.no)) { _, _ ->
                    notifyDetailFragment(false)
                }
                .show()
        } else {
            requestSmsPermission()
        }
    }

    private fun requestSmsPermission() {
        ActivityCompat.requestPermissions(this,
            arrayOf(sendSmsPermission),
            PERMISSION_SEND_SMS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray) {

        when(requestCode) {
            PERMISSION_SEND_SMS -> checkGrantPermission(grantResults)
        }
    }

    private fun checkGrantPermission(grantResults: IntArray) {
        if (grantResults.isNotEmpty() &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            notifyDetailFragment(true)
        } else {
            notifyDetailFragment(false)
        }
    }

    private fun notifyDetailFragment(permissionGranted: Boolean) {

        val activeFragment = fragment.childFragmentManager.primaryNavigationFragment
        if(activeFragment is DetailFragment) {
            activeFragment.onPermissionResult(permissionGranted)
        }

    }

}