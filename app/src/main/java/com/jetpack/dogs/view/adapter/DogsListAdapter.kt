package com.jetpack.dogs.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.jetpack.dogs.databinding.ItemDogBinding
import com.jetpack.dogs.model.DogBreed
import com.jetpack.dogs.view.fragment.ListFragmentDirections
import javax.inject.Inject

class DogsListAdapter @Inject constructor(private val layoutInflater: LayoutInflater) :
    RecyclerView.Adapter<DogsListAdapter.DogViewHolder>() {

    private lateinit var binding: ItemDogBinding

    private var list: MutableList<DogBreed> = mutableListOf()

    fun updateList(dogsList: List<DogBreed>) {
        list.apply {
            clear()
            addAll(dogsList)
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        binding = ItemDogBinding.inflate(layoutInflater)
        return DogViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    class DogViewHolder(private val binding: ItemDogBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(itemDog: DogBreed) {

            val action = ListFragmentDirections.actionDetailFragment().apply {
                dogUuid = itemDog.uuid
            }

            binding.apply {
                dog = itemDog
                itemDogContainer.setOnClickListener {
                    Navigation.findNavController(it).navigate(action)
                }
            }

        }
    }

}