package com.jetpack.dogs.view.fragment

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.telephony.SmsManager
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.palette.graphics.Palette
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.jetpack.dogs.R
import com.jetpack.dogs.databinding.FragmentDetailBinding
import com.jetpack.dogs.databinding.SendSmsDialogBinding
import com.jetpack.dogs.model.DogBreed
import com.jetpack.dogs.model.DogPalette
import com.jetpack.dogs.model.SmsInfo
import com.jetpack.dogs.view.activity.MainActivity
import com.jetpack.dogs.viewModel.DetailViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class DetailFragment : DaggerFragment() {

    @Inject
    lateinit var glide: RequestManager

    private lateinit var binding: FragmentDetailBinding
    private lateinit var currentDog: DogBreed
    private val viewModel: DetailViewModel by viewModels()
    private var sendSmsStarted = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var dogUuid = 0

        arguments?.let {
            dogUuid = DetailFragmentArgs.fromBundle(it).dogUuid
        }

        binding.viewModel = this.viewModel

        viewModel.refresh(dogUuid)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            dogDetail.observe(viewLifecycleOwner, { retrievedDog ->
                currentDog = retrievedDog
                retrievedDog.imageUrl?.let {
                    setUpBackgroundColor(it)
                }
            })
        }
    }

    private fun setUpBackgroundColor(url: String) {
        glide.asBitmap().load(url).into(object : CustomTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                Palette.from(resource).generate { palette ->
                    val color = palette?.lightMutedSwatch?.rgb ?: 0
                    binding.palette = DogPalette(color)
                }
            }

            override fun onLoadCleared(placeholder: Drawable?) {}
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_send_sms -> {
                sendSmsStarted = true
                (activity as MainActivity).checkSmsPermission()
            }

            R.id.action_share -> {
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_SUBJECT, "Check out this dog breed")
                    putExtra(Intent.EXTRA_TEXT, "${currentDog.dogBreed} bred for ${currentDog.bredFor}")
                    putExtra(Intent.EXTRA_STREAM, currentDog.imageUrl)

                    startActivity(Intent.createChooser(this, "Share with"))
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun onPermissionResult(permissionGranted: Boolean) {
        if (sendSmsStarted && permissionGranted) {
            context?.let {
                val smsInfo = SmsInfo(
                    "",
                    "${currentDog.dogBreed} bred for ${currentDog.bredFor}",
                    currentDog.imageUrl
                )

                val dialogBinding = DataBindingUtil.inflate<SendSmsDialogBinding>(
                    LayoutInflater.from(it),
                    R.layout.send_sms_dialog,
                    null,
                    false
                )

                AlertDialog.Builder(it)
                    .setView(dialogBinding.root)
                    .setPositiveButton("Send SMS") { _, _ ->
                        if(!dialogBinding.smsDestination.text.isNullOrEmpty()) {
                            smsInfo.to = dialogBinding.smsDestination.text.toString()
                            sendSms(smsInfo)
                        }
                    }
                    .setNegativeButton("Cancel") { _, _ -> }
                    .show()

                dialogBinding.smsInfo = smsInfo
            }
        }
    }

    private fun sendSms(smsInfo: SmsInfo) {
        val intent = Intent(context, MainActivity::class.java)
        val pi = PendingIntent.getActivity(context, 0, intent, 0)

        SmsManager.getDefault()
          .sendTextMessage(smsInfo.to, null, smsInfo.text, pi, null)

    }

}