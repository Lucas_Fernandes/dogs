package com.jetpack.dogs.view.fragment

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.jetpack.dogs.R
import com.jetpack.dogs.databinding.FragmentListBinding
import com.jetpack.dogs.di.factory.ViewModelFactory
import com.jetpack.dogs.view.adapter.DogsListAdapter
import com.jetpack.dogs.viewModel.ListViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var dogsListAdapter: DogsListAdapter

    private lateinit var binding: FragmentListBinding
    private val viewModel: ListViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        binding = FragmentListBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.refresh()

        binding.apply {
            vm = viewModel

            dogsList.also { rv ->
                rv.layoutManager = LinearLayoutManager(context)
                rv.adapter = dogsListAdapter
            }

            swipeRefreshLayout.also { srl ->
                srl.setOnRefreshListener {
                    viewModel.refreshBypassCache()
                    srl.isRefreshing = false
                }
            }
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            dogsListResult.observe(viewLifecycleOwner, { list ->
                list?.let { dogsListAdapter.updateList(list) }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.actionSettings -> {
                view?.let {
                    Navigation.findNavController(it)
                              .navigate(ListFragmentDirections.actionSettingsFragment())
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }
}