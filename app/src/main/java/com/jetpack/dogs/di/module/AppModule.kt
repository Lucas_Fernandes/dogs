package com.jetpack.dogs.di.module

import android.app.Application
import android.content.Context
import android.view.LayoutInflater
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.jetpack.dogs.R
import com.jetpack.dogs.view.adapter.DogsListAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideLayoutInflater(application: Application): LayoutInflater =
        application.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    @Singleton
    @Provides
    fun provideCircularProgressDrawable(application: Application): CircularProgressDrawable {
        return CircularProgressDrawable(application).apply {
            strokeWidth = 10f
            centerRadius = 50f
            start()
        }
    }

    @Singleton
    @Provides
    fun provideDogsListRecyclerViewAdapter(layoutInflater: LayoutInflater):
            DogsListAdapter = DogsListAdapter(layoutInflater)
}