package com.jetpack.dogs.di.module

import android.app.Application
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.jetpack.dogs.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class GlideModule {
    @Singleton
    @Provides
    fun provideRequestOptions(circularProgressDrawable: CircularProgressDrawable): RequestOptions =
        RequestOptions().placeholder(circularProgressDrawable).error(R.mipmap.ic_dog_icon)

    @Singleton
    @Provides
    fun provideGlideInstance(application: Application, requestOptions: RequestOptions): RequestManager =
        Glide.with(application).setDefaultRequestOptions(requestOptions)

}