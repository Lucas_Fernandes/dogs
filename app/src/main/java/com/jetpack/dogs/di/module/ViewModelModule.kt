package com.jetpack.dogs.di.module

import androidx.lifecycle.ViewModel
import com.jetpack.dogs.di.annotation.ViewModelKey
import com.jetpack.dogs.viewModel.DetailViewModel
import com.jetpack.dogs.viewModel.ListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindListViewModel(listViewModel: ListViewModel): ViewModel

}