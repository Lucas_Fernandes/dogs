package com.jetpack.dogs.di.component

import androidx.databinding.DataBindingComponent
import com.jetpack.dogs.databinding.ImageBindingAdapter
import com.jetpack.dogs.di.annotation.DataBinding
import com.jetpack.dogs.di.module.BindingModule
import dagger.Component

@DataBinding
@Component(dependencies = [AppComponent::class], modules = [BindingModule::class])
interface BindingComponent : DataBindingComponent {
    override fun getImageBindingAdapter(): ImageBindingAdapter
}