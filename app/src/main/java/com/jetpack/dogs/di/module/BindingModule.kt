package com.jetpack.dogs.di.module

import com.bumptech.glide.RequestManager
import com.jetpack.dogs.databinding.ImageBindingAdapter
import com.jetpack.dogs.di.annotation.DataBinding
import dagger.Module
import dagger.Provides

@Module
class BindingModule {
    @Provides
    @DataBinding
    fun provideImageBindingAdapter(glide: RequestManager) = ImageBindingAdapter(glide)
}