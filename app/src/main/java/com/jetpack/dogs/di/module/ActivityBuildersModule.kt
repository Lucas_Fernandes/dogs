package com.jetpack.dogs.di.module

import com.jetpack.dogs.view.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(
        modules = [
            FragmentBuildersModule::class,
        ]
    )
    abstract fun contributeMainActivity(): MainActivity
}