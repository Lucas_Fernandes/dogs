package com.jetpack.dogs.di.component

import android.app.Application
import com.bumptech.glide.RequestManager
import com.jetpack.dogs.application.BaseApplication
import com.jetpack.dogs.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        AppModule::class,
        ViewModelModule::class,
        RetrofitModule::class,
        GlideModule::class
    ]
)
interface AppComponent : AndroidInjector<BaseApplication> {

    fun requestManager(): RequestManager

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}