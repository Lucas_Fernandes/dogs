package com.jetpack.dogs.di.module

import com.jetpack.dogs.view.fragment.DetailFragment
import com.jetpack.dogs.view.fragment.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeListFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailFragment(): DetailFragment
}