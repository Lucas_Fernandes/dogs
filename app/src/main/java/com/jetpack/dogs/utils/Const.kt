package com.jetpack.dogs.utils

object Const {
    const val BASE_URL = "https://raw.githubusercontent.com"
    const val CHANNEL_ID = "Dogs channel id"
    const val NOTIFICATION_ID = 123
    const val PERMISSION_SEND_SMS = 123
}