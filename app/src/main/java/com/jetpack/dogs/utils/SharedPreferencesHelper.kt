package com.jetpack.dogs.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class SharedPreferencesHelper {

    companion object {
        private const val PREF_TIME = "Pref Time"
       lateinit var prefs: SharedPreferences

        @Volatile private var instance: SharedPreferencesHelper? = null
        private val LOCK = Any()

        operator fun invoke(context: Context): SharedPreferencesHelper =
            instance ?: synchronized(LOCK) {
                instance ?: builderHelper(context).also {
                    instance = it
                }
            }

        private fun builderHelper(context: Context): SharedPreferencesHelper {
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return SharedPreferencesHelper()
        }

    }

    fun saveUpdateTime(time: Long) {
        prefs.edit(commit = true) {
            putLong(PREF_TIME, time)
        }
    }

    fun geUpdateTime() = prefs.getLong(PREF_TIME, 0)

    fun getCacheDuration() = prefs.getString("pref_cache_duration", "")

    fun checkNight() {
        val isNightModeOn = prefs.getBoolean("NIGHT", false)

        if(isNightModeOn) {

        }

    }



}