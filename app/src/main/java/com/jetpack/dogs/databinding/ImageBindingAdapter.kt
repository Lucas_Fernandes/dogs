package com.jetpack.dogs.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.RequestManager
import javax.inject.Inject

class ImageBindingAdapter @Inject constructor(private val glide: RequestManager) {

    @BindingAdapter("android:imageUrl")
    fun loadImage(view: ImageView, url: String?) {
        url?.let {
            glide.load(url).into(view)
        }
    }

}