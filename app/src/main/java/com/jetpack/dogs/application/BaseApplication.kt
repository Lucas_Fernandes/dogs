package com.jetpack.dogs.application

import androidx.databinding.DataBindingUtil
import com.jetpack.dogs.di.component.DaggerAppComponent
import com.jetpack.dogs.di.component.DaggerBindingComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class BaseApplication : DaggerApplication() {
     
    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {

        val appComponent = DaggerAppComponent.builder()
            .application(this@BaseApplication)
            .build()

        val bindingComponent = DaggerBindingComponent
            .builder()
            .appComponent(appComponent)
            .build()

        DataBindingUtil.setDefaultComponent(bindingComponent)

        return appComponent
    }

}