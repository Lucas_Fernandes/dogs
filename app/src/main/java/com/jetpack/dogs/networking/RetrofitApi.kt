package com.jetpack.dogs.networking

import com.jetpack.dogs.model.DogBreed
import io.reactivex.Single
import retrofit2.http.GET

interface RetrofitApi {

    @GET("/DevTides/DogsApi/master/dogs.json")
    fun getDogs(): Single<List<DogBreed>>

}