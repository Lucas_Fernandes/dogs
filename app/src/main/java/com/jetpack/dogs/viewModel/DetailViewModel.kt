package com.jetpack.dogs.viewModel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.jetpack.dogs.model.DogBreed
import com.jetpack.dogs.room.database.DogDatabase
import kotlinx.coroutines.launch

class DetailViewModel(application: Application) : BaseViewModel(application) {

    val dogDetail = MutableLiveData<DogBreed>()

    fun refresh(uuid: Int) {
        fetchDogDetail(uuid)
    }

    private fun fetchDogDetail(uuid: Int) {
        launch {
            val dog = DogDatabase(getApplication()).dogDao().getDog(uuid)
            dogDetail.value = dog
        }
    }

}