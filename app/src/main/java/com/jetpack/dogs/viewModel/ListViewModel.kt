package com.jetpack.dogs.viewModel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.jetpack.dogs.model.DogBreed
import com.jetpack.dogs.networking.RetrofitApi
import com.jetpack.dogs.room.database.DogDatabase
import com.jetpack.dogs.utils.NotificationsHelper
import com.jetpack.dogs.utils.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListViewModel
@Inject constructor(application: Application, private val apiService: RetrofitApi) :
    BaseViewModel(application) {

    private var refreshTime = 5 * 60 * 1000 * 1000 * 1000L
    private var prefHelper = SharedPreferencesHelper(application)
    private val disposable = CompositeDisposable()
    val dogsListResult = MutableLiveData<List<DogBreed>>()
    val loadingError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun refresh() {
        checkCacheDuration()
        prefHelper.geUpdateTime()?.let { fetchedTimeFromDB ->
            if (fetchedTimeFromDB != 0L && System.nanoTime() - fetchedTimeFromDB < refreshTime) {
                fetchFromDatabase()
            } else {
                fetchFromRemote()
            }
        }
    }

    private fun checkCacheDuration() {
        val cachePreference = prefHelper.getCacheDuration()
        try {
            val cachePreferenceInt = cachePreference?.toInt() ?: 5 * 60
            refreshTime = cachePreferenceInt.times(1000 * 1000 * 1000L)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    fun refreshBypassCache() {
        fetchFromRemote()
    }

    private fun fetchFromRemote() {
        loading.value = true
        disposable.add(
            apiService.getDogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<DogBreed>>() {
                    override fun onSuccess(dogsList: List<DogBreed>) {
                        loading.value = false
                        loadingError.value = false
                        storeDogsLocally(dogsList)

                        Toast.makeText(
                            getApplication(),
                            "Retrieved from endpoint", Toast.LENGTH_SHORT
                        ).show()

                        NotificationsHelper(getApplication()).createNotification()
                    }

                    override fun onError(e: Throwable) {
                        loading.value = false
                        loadingError.value = true
                    }
                })
        )
    }

    private fun fetchFromDatabase() {
        loading.value = true
        launch {
            val dogs = DogDatabase(getApplication()).dogDao().getAllDogs()
            dogsRetrieved(dogs)
            Toast.makeText(getApplication(), "Retrieved from database", Toast.LENGTH_SHORT).show()
        }
    }

    private fun storeDogsLocally(list: List<DogBreed>) {
        launch {
            var i = 0
            DogDatabase(getApplication()).dogDao().apply {
                deleteAllDogs()
                val result = insertAll(*list.toTypedArray())

                while (i < list.size) {
                    list[i].uuid = result[i].toInt()
                    ++i
                }

                dogsRetrieved(list)
            }
        }
        prefHelper.saveUpdateTime(System.nanoTime())
    }

    private fun dogsRetrieved(dogsList: List<DogBreed>) {
        dogsListResult.value = dogsList
        loading.value = false
        loadingError.value = false
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

}