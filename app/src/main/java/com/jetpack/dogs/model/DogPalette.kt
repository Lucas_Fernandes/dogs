package com.jetpack.dogs.model

data class DogPalette(val color: Int)