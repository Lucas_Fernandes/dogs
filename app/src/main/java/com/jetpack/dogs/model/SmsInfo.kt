package com.jetpack.dogs.model

data class SmsInfo(
    var to: String,
    val text: String,
    val imageUrl: String?,
)